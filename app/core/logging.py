from functools import cache

from app import settings

NAME = "api"


@cache
def get_logging_config() -> dict:
    """Cache and return logging configuration dictionary."""

    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                "()": "uvicorn.logging.DefaultFormatter",
                "fmt": "[%(name)-15s] %(levelprefix)s %(asctime)s %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
            "access": {
                "()": "uvicorn.logging.AccessFormatter",
                "fmt": "[%(asctime)-15s] %(levelprefix)s %(asctime)s %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            "default": {
                "formatter": "default",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stderr",
            },
            "access": {
                "formatter": "access",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
            },
        },
        "loggers": {
            NAME: {
                "handlers": ["default"],
                "level": settings.loglevel,
            },
            "fastapi": {
                "handlers": ["default"],
                "level": settings.loglevel,
                "propagate": False,
                "qualname": "fastapi.logger.logger",
            },
            "uvicorn.access": {
                "handlers": ["default"],
                "level": settings.loglevel,
                "propagate": False,
            },
            "uvicorn.error": {
                "handlers": ["default"],
                "level": settings.loglevel,
                "propagate": False,
            },
        },
    }
