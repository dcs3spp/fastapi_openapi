"""Settings configuration model."""

from importlib import metadata
from pydantic import BaseModel, BaseSettings

_name = "skeleton_fastapi"
_metadata = metadata.metadata(_name)
_version = _metadata["version"]
_major_version = _version[0]


class MetaData(BaseModel):
    """Model that encaspulates package metadata populated via importlib."""

    description: str = _metadata["summary"]
    name: str = _name
    version: str = _version
    version_prefix: str = f"v{_major_version}"


class CorsSettings(BaseModel):
    """Model that encapsulates cors settings."""

    allow_credentials: bool = False
    allow_headers: list[str] = ["*"]
    allow_methods: list[str] = ["*"]
    allow_origins: list[str] = ["*"]


class Settings(BaseSettings):
    """Settings configured from environment variables."""

    cors: CorsSettings
    debug: bool = False
    loglevel: str = "INFO"
    metadata: MetaData
    reload: bool = False
