from app.core.config import CorsSettings, MetaData, Settings

settings = Settings(cors=CorsSettings(), metadata=MetaData())
