import json

from dataclasses import dataclass
from fastapi import Query
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from pydantic.json import pydantic_encoder
from pydantic.types import Json


@dataclass
class CalculationParams:
    """Model that accepts mathematcal calculation request."""

    operand_a: int = Query(1, description="The first operand", example=2)
    operand_b: int = Query(1, description="The second operand", example=3)
    operation: str = Query(
        "+",
        description=("The mathematical operation." " Can be +, -, *, or /"),
        example="-",
    )

    class Config:
        schema_extra = {
            "example": {
                "operand_a": 2,
                "operand_b": 3,
                "operation": "+",
            }
        }


class CalculationResponse(BaseModel):
    """Model that returns calculation response."""

    input_data: Json
    result: float

    class Config:
        schema_extra = {
            "example": {
                "input_data": {"operand_a": 2, "operand_b": 3, "operation": "-"},
                "result": -1,
            }
        }


calc_router = APIRouter()


@calc_router.get(
    description=(
        "Perform a mathematical calculation of addition,"
        " subtraction, multiplication or division"
    ),
    path="/",
    response_model=CalculationResponse,
    responses={
        200: {
            "description": "Success! Response contains the calculation result",
        },
        422: {"description": "Invalid symbol. Use +, -, * or /"},
    },
    tags=["Calculations"],
)
async def calc(
    calculation: CalculationParams = Depends(CalculationParams),
) -> JSONResponse:
    result = 0

    if calculation.operation.lower() == "+":
        result = calculation.operand_a + calculation.operand_b
    elif calculation.operation.lower() == "-":
        result = calculation.operand_a - calculation.operand_b
    elif calculation.operation.lower() == "*":
        result = calculation.operand_a * calculation.operand_b
    elif calculation.operation.lower() == "/":
        result = calculation.operand_a / calculation.operand_b
    else:
        raise HTTPException(
            status_code=422, detail=f"Invalid symbol {calculation.operation}"
        )

    json_data = json.dumps(calculation, indent=4, default=pydantic_encoder)

    response = CalculationResponse(result=result, input_data=json_data)

    return JSONResponse(status_code=200, content=response.dict())
