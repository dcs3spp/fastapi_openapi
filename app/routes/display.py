from fastapi import APIRouter
from pydantic import BaseModel

display_router = APIRouter()


class EchoName(BaseModel):
    """Model that echoes full name response for display endpoint."""

    fullname: str

    class Config:
        schema_extra = {"example": {"fullname": "Bertha Smith"}}


class Name(BaseModel):
    """Model that accepts name request to echo back to client."""

    forename: str
    surname: str
    title: str

    class Config:
        schema_extra = {
            "example": {
                "forename": "Bertha",
                "surname": "Smith",
                "title": "Miss",
            }
        }


@display_router.post(
    description=(
        "Accepts title, forename and surname in payload and returns "
        "fullname in response"
    ),
    path="/",
    response_model=EchoName,
    responses={
        200: {
            "description": "Success! Response contains full name",
        },
        422: {"description": "Invalid payload data"},
    },
    tags=["Echo"],
)
async def display(name: Name) -> EchoName:
    full_name = f"{name.title} {name.forename} {name.surname}"

    return EchoName(fullname=full_name)
