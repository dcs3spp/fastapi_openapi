from fastapi import APIRouter
from pydantic import BaseModel

from app import settings

health_router = APIRouter()


class HealthCheck(BaseModel):
    """Model for health status reponse."""
    description: str
    name: str
    version: str

    class Config:
        schema_extra = {
            "example": {
                "description": "The application description",
                "name": "The application name",
                "version": "1.0",
            }
        }

@health_router.get(
    "/",
    description="Query health status of service",
    responses={
        200: {
            "description": (
                "Success! Response contains application name, "
                "version and description"
            )
        },
    },
    response_model=HealthCheck,
    tags=["Health"],
)
async def health_check():
    return {
        "name": settings.metadata.name,
        "description": settings.metadata.description,
        "version": settings.metadata.version,
    }
