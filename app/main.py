import logging
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from logging.config import dictConfig

from app import settings
from app.core.logging import get_logging_config, NAME
from app.routes.calc import calc_router
from app.routes.display import display_router
from app.routes.health import health_router


dictConfig(get_logging_config())

logger = logging.getLogger(NAME)

tags_metadata = [
    {
        "name": "Calculations",
        "description": "Perform mathematical calculations.",
    },
    {
        "name": "Echo",
        "description": "Echo back input data.",
    },
    {"name": "Health", "description": "Query health status of service."},
]


def _shutdown() -> None:
    """Shutdown event handler."""

    logger.info("running shutdown event")
    logger.info("shutdown event complete")


def _startup() -> None:
    """Startup event handler."""

    logger.info("running startup event")
    logger.debug(f"cors:allow credentials={settings.cors.allow_credentials}")
    logger.debug(f"cors:allow headers={settings.cors.allow_headers}")
    logger.debug(f"cors:allow methods={settings.cors.allow_methods}")
    logger.debug(f"cors:allow origins={settings.cors.allow_origins}")
    logger.info(f"service version is {settings.metadata.version}")


def create_app() -> FastAPI:
    """Create a FastAPI service.

    Setup cors middleware, routers and add logger to app state context
    """

    the_app = FastAPI(
        debug=settings.debug,
        description=f"{settings.metadata.description}",
        docs_url=f"/{settings.metadata.version_prefix}/docs",
        on_shutdown=[_shutdown],
        on_startup=[_startup],
        openapi_tags=tags_metadata,
        redoc_url=f"/{settings.metadata.version_prefix}/redoc",
        title="Swagger Demo",
        version=settings.metadata.version,
        openapi_url=f"/{settings.metadata.version_prefix}/openapi.json",
    )

    the_app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors.allow_origins,
        allow_credentials=settings.cors.allow_origins,
        allow_methods=settings.cors.allow_methods,
        allow_headers=settings.cors.allow_methods,
    )

    the_app.include_router(calc_router, prefix="/calc")
    the_app.include_router(display_router, prefix="/display")
    the_app.include_router(health_router, prefix="/health")

    the_app.state.logger = logger

    return the_app


def start() -> None:
    """Start the server."""

    uvicorn.run(
        "app.main:create_app",
        factory=True,
        host="0.0.0.0",
        port=8080,
        reload=settings.reload,
    )


if __name__ == "__main__":
    start()
