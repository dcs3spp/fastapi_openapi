<div align="center">
  ![Logo](https://images.unsplash.com/photo-1534357192615-81138dca8f00?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=240&q=80)
  <center><h1>FastAPI OpenAPI Demo</h1></center>
</div>

<br/>

<div align="center">

<b>OpenAPI Documentation Demonstration</b>

[![GitHub license](https://img.shields.io/badge/license-GPL-blue.svg)](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/blob/master/LICENSE.txt)
[![pipeline status](https://gitlab.com/dcs3spp/portfolio/badges/master/pipeline.svg)](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/pipelines)

</div>

# 🌎 Overview

This is a demonstration of a small FASTAPI service with the following endpoints:

- **/health**: health endpoint that returns the project name, description and application version.
- **/v{major_version_number}/calc**: accepts operands and an operand in request payload. Performs the
  calculation and returns the result in the response.
- **/v{major_version_number}/display**: accepts a title, forename and surname in request payload and
  returns a fullname string in the response payload.

All endpoints are prefixed with the major version number, which is extracted from
the `pyproject.toml` file. For example, if the version in toml file is _0.1.0_,
then the calc endpoint is _/v0/calc_.

It serves to demonstrate how to generate OpenAPI (Swagger) documentation from
FASTAPI endpoints. Specifically, it encompasses examples of the following:

- Extract package metadata (description, version and name) from `pyproject.toml`
  file via `importlib.metadata`.
- Document query endpoint parameters.
- Document example query endpoint parameters.
- Document an endpoint description.
- Document example request and response payload via pydantic model.
- Document response status code description.

This README explains how to setup and configure the project. The OpenAPI
documentation can be found at the default location of
_http://localhost:8080/v0/docs_

<div align="center">
    ![Demo](https://i.ibb.co/mB5d46b/Open-APIDemo.png)
</div>

## 🔎 Prerequisites

- Python 3.11+
- [Poetry](https://python-poetry.org/docs/)

## ⚙️ Setup

1. Create a python virtual environment: `python -m venv .venv`
2. Activate the python virtual environment: `source .venv/bin/activate`
3. Install the dependencies: `poetry install`
4. Run the demo service: `make run`
5. Browse to the default OpenAPI docs location at:
   `http://localhost:8080/v0/docs`
6. Browse to the default redoc location at: `http://localhost:8080/v0/redoc`

## 🛠 Configuration Variables

Configuration is achieved via environment variables. Settings for CORS and
development can be provided.

### 🧯 CORS

| Environment variable     | Description                                                    | Default |
| :----------------------- | :------------------------------------------------------------- | :------ |
| cors\_\_alow_credentials | if True, cookies are allowed on cross-origin requests          | False   |
| cors\_\_allow_headers    | list of http request headers allowed for cross-origin requests | ["*"]   |
| cors\_\_allow_methods    | list of http methods allowed for cross-origin requests         | ["*"]   |
| cors_allow_origins       | list of origins allowed for cross-origin requests              | ["*"]   |

### 🏗 Development

| Environment variable | Description                                                      | Default | Makefile Default |
| :------------------- | :--------------------------------------------------------------- | :------ | :--------------- |
| debug                | is debugging enabled for fastAPI?                                | False   | True             |
| loglevel             | logging level: [DEBUG\|INFO\|WARNING\|ERROR\|CRITICAL]           | INFO    | DEBUG            |
| reload               | if True, enable watchdog to reload server if source files change | False   | True             |
